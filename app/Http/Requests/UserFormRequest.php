<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|max:63|unique:users,username,' . (int)$this->route('id'),
            'email' => 'required|email|max:255|unique:users,email,' . (int)$this->route('id'),
            'name' => 'required|max:63',
            'surname' => 'required|max:63',
            'password' => 'required|same:confirm-password',
            'confirm-password' => 'required',
            'phone' => 'required|max:20',
        ];
    }
}
