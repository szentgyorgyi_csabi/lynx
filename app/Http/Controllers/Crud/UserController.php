<?php

namespace App\Http\Controllers\Crud;

use App\Http\Controllers\Controller;
use App\Http\Requests\UserFormRequest;
use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    protected $paginateCount = 10;

    public function list(Request $request)
    {
        $users = [];
        $term = $request->input('search', '');

        if ($term) {
            $users = User::search($term)->paginate($this->paginateCount);
        } else {
            $users = User::paginate($this->paginateCount);
        }

        return view('admin.user.list', ['users' => $users]);
    }

    public function new()
    {
        return view('admin.user.form', ['user' => null]);
    }

    public function store(UserFormRequest $request)
    {
        $user = new User;
        $this->fillUser($user, $request);
        $request->session()->flash('message', trans('admin.user.created'));

        return redirect()->route('user-edit', ['id' => $user->id]);
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);

        return view('admin.user.form', ['user' => $user]);
    }

    public function save($id, UserFormRequest $request)
    {
        $user = User::findOrFail($id);
        $this->fillUser($user, $request);
        $request->session()->flash('message', trans('admin.user.updated'));

        return redirect()->back();
    }

    public function delete(Request $request, $id = 0)
    {
        $count = $id ? User::destroy($id) : User::destroy((array)$request->input('ids'));
        $request->session()->flash('message', trans('admin.user.deleted', ['count' => $count]));

        return redirect()->back();
    }

    public function ajaxSearch(Request $request)
    {
        $users = User::search($request->input('term', ''))->get()->toArray();
        $response = array_map(function ($item) {
            return [
                'value' => $item['id'],
                'label' => $item['username'],
                'href' => route('user-edit', ['id' => $item['id']]),
            ];
        }, $users);

        return response()->json($response);
    }

    // no reason to return anything
    public function ajaxValidate(UserFormRequest $request)
    {
    }

    protected function fillUser(user $user, Request $request)
    {
        $user->fill($request->all());
        $user->password = bcrypt($request->input('password'));
        $user->save();
    }
}
