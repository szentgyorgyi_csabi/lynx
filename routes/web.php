<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::group(['middleware' => ['web', 'auth']], function () {
    // landing page
    Route::get('/', ['as' => 'landing-page', 'uses' => 'HomeController@index']);

    // User CRUD
    Route::get('/user/create', ['as' => 'user-new', 'uses' => 'Crud\UserController@new']);
    Route::post('/user/create', ['as' => 'user-store', 'uses' => 'Crud\UserController@store']);
    Route::get('/user/update/{id}', ['as' => 'user-edit', 'uses' => 'Crud\UserController@edit'])->where(['id' => '[0-9]+']);
    Route::post('/user/update/{id}', ['as' => 'user-save', 'uses' => 'Crud\UserController@save'])->where(['id' => '[0-9]+']);
    Route::post('/user/delete/{id?}', ['as' => 'user-delete', 'uses' => 'Crud\UserController@delete'])->where(['id' => '[0-9]+']);
    Route::get('/user/search', ['as' => 'user-search', 'uses' => 'Crud\UserController@ajaxSearch']);
    Route::post('/user/validate/{id}', ['as' => 'user-validate', 'uses' => 'Crud\UserController@ajaxValidate'])->where(['id' => '[0-9]+']);
    Route::get('/user', ['as' => 'user-list', 'uses' => 'Crud\UserController@list']);
});
