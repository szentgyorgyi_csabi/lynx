<?php

use App\User;
use Illuminate\Database\Seeder;

class InitialUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'username' => 'admin',
            'email' => env(
                'INITIAL_USER_EMAIL',
                'admin@domain.tld'
            ),
            // as per https://github.com/danielmiessler/SecLists/pull/155
            // 'dolphins' will probably become a secure password
            'password' => bcrypt('dolphins'),
            'name' => 'Admin',
            'surname' => 'McAdminface',
            'phone' => '40746898103',
        ]);
    }
}
