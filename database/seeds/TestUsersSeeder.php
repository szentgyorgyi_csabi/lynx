<?php

use App\User;
use Faker\Generator;
use Illuminate\Database\Seeder;

class TestUsersSeeder extends Seeder
{
    protected $insertCount = 42;

    protected $faker;

    public function __construct(Generator $faker)
    {
        $this->faker = $faker;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $entries = [];

        for ($i = 0; $i < $this->insertCount; $i++) {
            $entries[] = [
                'username' => $this->faker->unique()->userName,
                'email' => $this->faker->unique()->email,
                'password' => $this->faker->password,
                'name' => $this->faker->firstName,
                'surname' => $this->faker->lastName,
                'phone' => $this->faker->e164PhoneNumber,
            ];
        }

        User::insert($entries);
    }
}
