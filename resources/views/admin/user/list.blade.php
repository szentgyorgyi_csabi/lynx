@extends('admin.dashboard')

@section('content')
<div class="card">
  <div class="card-header" data-background-color="purple">
    <h4 class="title">@lang('admin.user.users')</h4>
    {{-- <p class="category">Here is a subtitle for this table</p> --}}
  </div>
  <div class="card-content table-responsive">
    <div class="row">
      <div class="col-md-8">
        <button onClick="$('#multidelete').submit()" type="button" class="btn btn-danger">@lang('common.delete-selected')</button>
        <a href="{{ route('user-new') }}" class="btn btn-primary">@lang('common.new')</a>
      </div>
      <div class="col-md-4 text-right">
        {{ Form::open(['route' => 'user-list', 'method' => 'GET', 'id' => 'search']) }}
          <div class="form-group  is-empty" style="display: inline-block;">
            {{ Form::text('search', null, ['class' => 'form-control', 'placeholder' => trans('common.search')]) }}
            <span class="material-input"></span>
          </div>
          <button type="submit" class="btn btn-white btn-round btn-just-icon">
            <i class="material-icons">search</i>
            <div class="ripple-container"></div>
          </button>
        {{ Form::close() }}
      </div>
    </div>
    @if($users->count())
      {{ Form::open(['route' => 'user-delete', 'method' => 'POST', 'id' => 'multidelete']) }}
        <table class="table">
          <thead class="text-danger">
            <th>
              <div class="checkbox">
                <label>{{ Form::checkbox('check-all', null, false) }}</label>
              </div>
            </th>
            <th>@lang('admin.user.username')</th>
            <th>@lang('admin.user.email')</th>
            <th>@lang('admin.user.name')</th>
            <th>@lang('admin.user.surname')</th>
            <th></th>
          </thead>
          <tbody>
            @foreach($users as $user)
              <tr>
                <td>
                  <div class="checkbox">
                    <label>{{ Form::checkbox('ids[]', $user->id, false) }}</label>
                  </div>
                </td>
                <td>{{ $user->username }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->name }}</td>
                <td>{{ $user->surname }}</td>
                <td>
                  <a href="{{ route('user-edit', ['id' => $user->id]) }}" class="btn btn-primary pull-right">@lang('common.edit')</a>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      {{ Form::close() }}
      <div>{{ $users->links() }}</div>
    @else
      <p>@lang('admin.user.no-users')</p>
    @endif
  </div>
</div>
@endsection

@push('end_scripts')
  <script
    src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"
    integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="
    crossorigin="anonymous"></script>
  <script type="text/javascript">
    $(document).ready(function(){
      $('input[name="check-all"]').on('click', function(){
        $('input[name="ids[]"]').prop('checked', $(this).prop('checked'));
      });
      $('input[name="search"').autocomplete({
        minLength: 3,
        delay: 500,
        source: function(request, response){
          $.ajax({
            url: '{{ route('user-search') }}',
            type: 'GET',
            dataType: 'json',
            data: {term: request.term},
            success: function(data){
              response(data);
            },
          });
        },
        select: function(event, ui){
          event.preventDefault();
          window.location.href = ui.item.href;
        },
      });
    });
  </script>
@endpush
