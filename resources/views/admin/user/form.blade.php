@extends('admin.dashboard')

@section('content')
<div class="card">
  <div class="card-header" data-background-color="purple">
    <h4 class="title">
      @if($user)
        @lang('admin.user.edit')
      @else
        @lang('admin.user.create')
      @endif
    </h4>
  </div>
  <div class="card-content">
    {{ Form::model($user, ['id' => 'form']) }}
      <div class="row">
        <div class="col-md-6">
          <div class="form-group label-floating @if(!(old('username') || $user)) is-empty @endif">
            {{ Form::label('username', trans('admin.user.username'), ['class' => 'control-label']) }}
            {{ Form::text('username', null, ['class' => 'form-control', 'required' => 'required']) }}
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group label-floating @if(!(old('email') || $user)) is-empty @endif">
            {{ Form::label('email', trans('admin.user.email'), ['class' => 'control-label']) }}
            {{ Form::email('email', null, ['class' => 'form-control', 'required' => 'required']) }}
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group label-floating @if(!(old('name') || $user)) is-empty @endif">
            {{ Form::label('name', trans('admin.user.name'), ['class' => 'control-label']) }}
            {{ Form::text('name', null, ['class' => 'form-control', 'required' => 'required']) }}
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group label-floating @if(!(old('surname') || $user)) is-empty @endif">
            {{ Form::label('surname', trans('admin.user.surname'), ['class' => 'control-label']) }}
            {{ Form::text('surname', null, ['class' => 'form-control', 'required' => 'required']) }}
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group label-floating is-empty">
            {{ Form::label('password', trans('admin.user.password'), ['class' => 'control-label']) }}
            {{ Form::password('password', ['class' => 'form-control', 'required' => 'required']) }}
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group label-floating is-empty">
            {{ Form::label('confirm-password', trans('admin.user.confirm-password'), ['class' => 'control-label']) }}
            {{ Form::password('confirm-password', ['class' => 'form-control', 'required' => 'required']) }}
          </div>
        </div>
        <div class="col-md-6">
          <div class="form-group label-floating @if(!(old('phone') || $user)) is-empty @endif">
            {{ Form::label('phone', trans('admin.user.phone'), ['class' => 'control-label']) }}
            {{ Form::text('phone', null, ['class' => 'form-control', 'required' => 'required']) }}
          </div>
        </div>
      </div>
      <a href="{{ route('user-list') }}" class="btn btn-default pull-right">@lang('common.cancel')</a>
      <button type="submit" class="btn btn-primary pull-right">@lang('common.save')</button>
      <div class="clearfix"></div>
    {{ Form::close() }}
  </div>
</div>
@endsection

@push('end_scripts')
  <script type="text/javascript">
    $(document).ready(function(){
      var should_validate = true;
      $('#form').on('submit', function(e){
        if (!should_validate) {
          return;
        }
        var event_triggerer = $(this);
        var event = e;
        event.preventDefault();
        $('#messages').find('.alert').remove();
        $.ajax({
          url: '{{ route('user-validate', ['id' => $user ? $user->id : 0]) }}',
          type: 'POST',
          dataType: 'json',
          data: event_triggerer.serialize(),
          error: function(jqXHR, textStatus, errorThrown){
            if (jqXHR.hasOwnProperty('responseJSON') && jqXHR.responseJSON.hasOwnProperty('errors')) {
              for (err in jqXHR.responseJSON.errors) {
                for (i in jqXHR.responseJSON.errors[err]) {
                  // brace yourself
                  $('#messages').prepend('<div class="alert alert-danger"><div class="container-fluid"><div class="alert-icon"><i class="material-icons">error_outline</i></div><button type="button" class="close" data-dismiss="alert" aria-label="@lang('common.close')"><span aria-hidden="true"><i class="material-icons">clear</i></span></button><b>@lang('common.warning')&colon;</b> ' + jqXHR.responseJSON.errors[err][i] + '</div></div>');
                }
              }
            }
          },
          statusCode: {
            200: function(){
              should_validate = false;
              event_triggerer.trigger('submit');
            }
          }
        });
      });
    });
  </script>
@endpush
