@extends('layouts.admin')

@section('title')Dashboard | {{ config('app.name') }}@endsection

@section('main-content')
  @include('admin.sidebar')
  <div class="main-panel">
    @include('admin.navbar')
    <div class="content">
      @include('admin.messages')
      <div class="container-fluid">
        @yield('content')
      </div>
    </div>
  </div>
@endsection
