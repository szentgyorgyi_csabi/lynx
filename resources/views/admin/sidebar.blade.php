<div class="sidebar" data-color="purple" data-image="/img/sidebar-1.jpg">
  <div class="logo">
    <a href="{{ route('landing-page') }}" class="simple-text">{{ config('app.name') }}</a>
  </div>
  <div class="sidebar-wrapper">
    <ul class="nav">
      <li class="active">
        <a href="dashboard.html">
          <i class="material-icons">dashboard</i>
          <p>@lang('admin.dashboard')</p>
        </a>
      </li>
      <li>
        <a href="{{ route('user-list') }}">
          <i class="material-icons">person</i>
          <p>@lang('admin.user.users')</p>
        </a>
      </li>
    </ul>
  </div>
</div>
