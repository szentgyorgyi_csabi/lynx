<div id="messages">
  @if (count($errors))
    @foreach ($errors->all() as $error)
      <div class="alert alert-danger">
        <div class="container-fluid">
          <div class="alert-icon">
            <i class="material-icons">error_outline</i>
          </div>
          <button type="button" class="close" data-dismiss="alert" aria-label="@lang('common.close')">
            <span aria-hidden="true"><i class="material-icons">clear</i></span>
          </button>
          <b>@lang('common.warning')&colon;</b>
          {{ $error }}
        </div>
      </div>
    @endforeach
  @endif
  @if (Session::has('message'))
    <div class="alert alert-success">
      <div class="container-fluid">
        <div class="alert-icon">
          <i class="material-icons">check</i>
        </div>
        <button type="button" class="close" data-dismiss="alert" aria-label="@lang('common.close')">
          <span aria-hidden="true"><i class="material-icons">clear</i></span>
        </button>
        <b>@lang('common.success')&colon;</b>
        {{ Session::get('message') }}
      </div>
    </div>
  @endif
</div>
