@extends('layouts.admin')

@section('main-content')
<div class="container-fluid">
  <div class="row">
    <div class="col-xs-3 center-block" style="float: none">
      <div class="card">
        <div class="card-header" data-background-color="purple">
          <h4 class="title">@lang('common.login')</h4>
        </div>
        <div class="card-content">
          @include('admin.messages')
          {{ Form::open(['route' => 'login', 'method' => 'POST']) }}
            <div class="row">
              <div class="col-md-12">
                <div class="form-group label-floating @if(!(old('email'))) is-empty @endif">
                  {{ Form::label('email', trans('admin.user.email'), ['class' => 'control-label']) }}
                  {{ Form::email('email', old('email', ''), ['class' => 'form-control', 'required' => 'required']) }}
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group label-floating is-empty">
                  {{ Form::label('password', trans('admin.user.password'), ['class' => 'control-label']) }}
                  {{ Form::password('password', ['class' => 'form-control', 'required' => 'required']) }}
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-xs-12 text-center">
                <button type="submit" class="btn btn-primary">@lang('common.login')</button>
              </div>
            </div>
            <div class="clearfix"></div>
          {{ Form::close() }}
        </div>
      </div>
    </div>
  </div>
</div>
@endsection
